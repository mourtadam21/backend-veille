@extends('layouts.app')

@section('content')
    <div class="container">
        <h1> Dernières news sur l'écoresponsabilité numérique</h1>
        <div class="row justify-content-center" style="margin-top : 25px;">
            @foreach ($items as $item)
                <div class="col-md-4 mb-4">
                    <div class="card h-100">
                        <img src="{{ $item['image'] }}" class="card-img-top" alt="Article image" style="height: 150px; object-fit: cover;">
                        <div class="card-body">
                            <h5 class="card-title">{{ $item['title'] }}</h5>
                            <p class="card-text text-truncate" style="max-height: 75px; overflow: hidden;">{{ $item['description'] }}</p>
                        </div>
                        <div class="card-footer text-muted">
                            Publié le {{ $item['date'] }}
                        </div>
                        <a href="{{ $item['link'] }}" class="stretched-link"></a>
                    </div>
                </div>
            @endforeach
        </div>
        <div style="margin-top : 25px;">
            {{ $items->links('custom_pagination') }}
        </div>
    </div>
@endsection