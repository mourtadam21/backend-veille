<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <header class="mb-4" style="background-color: #008080;">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#" style="margin-left: 20px;">
                <img src="https://itgreen.eu/wp-content/uploads/2022/01/IT-GREEN.png" alt="Logo" height="100" style="padding: 10px;">
            </a>

            <form action="/search" method="get" class="form-inline my-2 my-lg-0 ml-auto" style="margin-right: 40px;">
                <input class="form-control mr-sm-2" type="search" name="search" placeholder="Rechercher..." aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher</button>
            </form>
        </nav>
    </header>

    <div id="app">
        <main class="py-4">
            <section id="podcast" class="container" style="border-radius: 5px; margin-bottom: 25px; margin-top : -10px;">
                <h1>Podcast</h1>
                <iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/666190698&color=%233eff72&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>
                <div style="font-size: 10px; color: #008080; text-align: center; line-break: anywhere; word-break: normal; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif; font-weight: 100;">
                    <a href="https://soundcloud.com/techologie" title="Techologie" target="_blank" style="color: #008080; text-decoration: none;">Techologie</a> · 
                    <a href="https://soundcloud.com/techologie/sets/tous-les-episodes" title="Tous les épisodes" target="_blank" style="color: #008080; text-decoration: none;">Tous les épisodes</a>
                </div>
            </section>
            @yield('content')
        </main>
    </div>
    
    <footer class="mt-4" style="background-color: rgb(40, 167, 69)">
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2); color :white;">
            © 2024 Green IT - Salomé, Mohamed, Laytou Hugo
        </div>
    </footer>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>