<p align="center"><img src="https://itgreen.eu/wp-content/uploads/2022/01/IT-GREEN.png" width="400" alt=GreenIT"></a></p>

# Veille technologique sur le Green IT

Pour notre veille technologique, nous avons mis en place un site web regroupant plusieurs sources d'informations, sélectionnées en ayant pris le soin de vérifier leur fiabilité à propos du sujet pour lequel nous souhaitions effectuer une mise en avant.

## Comment démarrer ?

Installation des dépendances avec composer (assurez vous d'avoir installer composer au préalable)
```
composer install
```
Configurer l'environnement 
```
cp .env.example .env
```
Génération de la clé d'application
```
php artisan key:generate
```
Créer une base de donnée du nom de "greenit" localement puis exécuter la commande suivante 
```
php artisan migrate
```
Pour finir, il ne vous reste plus qu'à démarrer le projet avec 
```
php artisan serve
```
