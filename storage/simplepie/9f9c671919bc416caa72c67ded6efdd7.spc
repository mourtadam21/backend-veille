a:6:{s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"feed";a:1:{i:0;a:6:{s:4:"data";s:43:"
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
";s:7:"attribs";a:1:{s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"base";s:2:"fr";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:15:"Blog IT's on us";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"subtitle";a:1:{i:0;a:5:{s:4:"data";s:39:"Blog traitant de numérique responsable";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:2:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:41:"https://www.itsonus.fr/blog/feed/feed.xml";s:3:"rel";s:4:"self";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"href";s:27:"https://www.itsonus.fr/blog";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2024-04-23T00:00:00Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:27:"https://www.itsonus.fr/blog";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:6:{s:4:"data";s:8:"
		
		
	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:2:{s:4:"name";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:5:"email";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}s:5:"entry";a:7:{i:0;a:6:{s:4:"data";s:17:"
		
		
		
		
		
	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:103:"Guide d'accessibilité pour développeur·euse web : débuter avec un lecteur d'écran sur une page web";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"href";s:76:"https://www.itsonus.fr/blog/article/guide_developpeuse-euse_lecteur_d_ecran/";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2024-04-23T00:00:00Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:76:"https://www.itsonus.fr/blog/article/guide_developpeuse-euse_lecteur_d_ecran/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:13543:"<p>En tant que développeur·euse web, quand on débute en accessibilité, on est rapidement confronté au besoin de pouvoir se plonger en condition réelle. Se mettre à la place de la personne qui a besoin d’un service accessible reste un bon moyen de mettre en place les bonnes conditions d’utilisabilité.</p>
<p>Parmi le panel d’outils à connaître, le lecteur d’écran est une des solutions utilisées par les personnes en situation de handicap. Mais voilà, la prise en main de ce genre d’outil n’est pas des plus évidentes, et peut même parfois rebuter une personne motivée.</p>
<p>Un lecteur d’écran est une application capable de restituer le contenu d’une page web (ou toute application d’ailleurs) via une voix synthétique, en s’appuyant sur la structure de la page, pour permettre à une personne en situation de handicap (majoritairement visuel, mais aussi moteur, auditif, cognitif…) de pouvoir comprendre comment l’information est organisée. L’outil permet également de fournir une aide pour interagir avec la page.</p>
<p>Oui mais voilà : au premier abord, un lecteur d’écran semble produire un flot ininterrompu de paroles de façon incontrôlable, et  pourrait en rebuter plus d’un. Je suis passé par cette phase où le lecteur d’écran était une sorte de monstre métamorphe difficile à dompter. Et pourtant, il suffit de pas grand chose pour commencer à comprendre comment s’en servir basiquement, et faire déjà de belles avancées.</p>
<p>Les quelques conseils qui vont suivre ne prennent que peu de temps à mettre en place, mais devraient déjà changer largement la façon dont vous utilisez le lecteur d’écran. Ce guide est loin d’être exhaustif, mais vous aurez quelques bases utiles pour son utilisation dans la vie quotidienne en tant que producteur de service numérique.</p>
<p>Cet article se focalise sur NVDA qui est mon outil du quotidien. Il fait généralement partie des lecteurs recommandés, mais n’est pas le seul. Je vais sauter la phase d’installation qui ne devrait pas poser trop de problèmes, et me focaliser sur son utilisation. Je vais également essentiellement me concentrer sur l’utilisation de NVDA dans le cadre d’une page web, bien que l’outil soit capable de bien plus que ça.</p>
<p>Vouloir tester une page web avec NVDA est une bonne chose. Mais lorsqu’on n’a pas l’habitude de s’en servir, la tâche peut se révéler ardue. Voici donc quelques conseils pour vous aider à rendre l’outil plus facile d’utilisation avant même de se lancer dans le grand bain.</p>
<h2 id="activer-la-mise-en-surbrillance-du-focus" tabindex="-1">Activer la mise en surbrillance du focus</h2>
<p>Quiconque a déjà lancé un lecteur d’écran sans y être habitué·e peut se retrouver rapidement perdu·e sans savoir exactement où le lecteur d’écran en est dans la page. Lorsqu’on n’a pas de handicap visuel, autant profiter des capacités supplémentaires de l’outil pour permettre de mieux s’y retrouver.</p>
<p>Il est possible de configurer NVDA pour qu’il mette en exergue (via un encadrement coloré) différents types de focus. Ces options se trouvent dans les préférences de NVDA. Pour y accéder, lancez NVDA. Par défaut, il va se retrouver minimisé dans les applications masquées sur Windows (sur Mac, je ne saurais pas dire) :</p>
<figure><img src="https://www.itsonus.fr/blog_images/articles/guide_de_survie_lecteur_d_ecran/image-1.png" alt="Applications en tâche de fond" loading="lazy" decoding="async"></figure>
<p>Applications en tâche de fond</p>
<p>Cliquez sur l’icône NVDA, puis sur le menu <strong>Préférences</strong>, puis sur <strong>Paramètres…</strong></p>
<p>Dans la fenêtre qui s’ouvre, allez ensuite dans la section <strong>Vision</strong>, comme suit :</p>
<figure><img src="https://www.itsonus.fr/blog_images/articles/guide_de_survie_lecteur_d_ecran/image-2.png" alt="Fenêtre de configuration de NVDA pour l'activation des options de focus" loading="lazy" decoding="async"></figure>
<p>Fenêtre de configuration de NVDA pour l’activation des options de focus</p>
<p>Je recommande de cocher ces options, qui ont les effets suivants :</p>
<ul>
<li><strong>Mettre en évidence le focus système</strong> : NVDA ajoute une bordure en pointillé bleu autour de l’élément qui a actuellement le focus système. Dans le cadre de la navigation web, le focus système est le focus qui est généralement posé sur les éléments nativement focusable de la page (boutons, liens, éléments de formulaire…).</li>
<li><strong>Mettre en évidence l’objet navigateur</strong> : NVDA ajoute une bordure rose autour de l’élément qui a actuellement le focus NVDA. Le focus NVDA se pose sur l’élément que NVDA est actuellement en train de vocaliser, ou celui sur lequel il s’est arrêté. Tout élément peut recevoir ce focus, et non uniquement les éléments focusables comme pour le focus système. La façon de déplacer le focus NVDA sera décrite plus tard dans l’article.</li>
<li><strong>Mettre en évidence le curseur du mode navigation</strong> : NVDA ajoute une bordure jaune autour du première caractère du bloc ou partie de bloc qu’il est en train de vocaliser. Ce curseur peut être utile lorsque NVDA est en train de vocaliser un long paragraphe. Par défaut, NVDA ne lit pas tout le paragraphe d’un coup, mais va le découper en morceaux. Ce curseur indique le début du morceau qu’il est en train de vocaliser ou qui a le focus NVDA.</li>
</ul>
<p>Avec tous ces focus actif, outre le beau sapin de Noël que ça produit, vous avez maintenant une meilleure compréhension de l’endroit où se trouve le focus de NVDA.</p>
<h2 id="journal-de-lecture" tabindex="-1">Journal de lecture</h2>
<p>Le journal de lecture est une fenêtre complémentaire qui retranscrit tout ce que la restitution vocalise. Cet outil permet notamment de mieux comprendre ce qui est dit, et de garder une trace de ce qui s’est passé. C’est notamment pratique pour une personne pas encore habituée à la façon dont les éléments sont restitués à l’oral par le lecteur d’écran.</p>
<p>A noter que l’exemple ci-dessous est un peu brouillon à cause de l’interférence avec l’outil de capture d’écran.</p>
<figure><img src="https://www.itsonus.fr/blog_images/articles/guide_de_survie_lecteur_d_ecran/image-3.png" alt="Démonstration de la visionneuse de parole sur une page de site e-commerce pour un casque audio" loading="lazy" decoding="async"></figure>
<p>Démonstration de la visionneuse de parole sur une page de site e-commerce pour un casque audio</p>
<p>Pour activer le journal de lecture, cliquez sur NVDA dans les applications masquées (comme pour l’ouverture des paramètres du paragraphe sur le focus), puis <strong>Outils</strong>, puis <strong>Visionneuse de parole</strong>.</p>
<h2 id="vitesse-de-vocalisation" tabindex="-1">Vitesse de vocalisation</h2>
<p>Une alternative au journal de vocalisation est de réduire la vitesse de la vocalisation. En effet, en jouant sur la vitesse de prononciation, notamment en la ralentissant, il est plus facile de comprendre ce qui est dit sans avoir à se reposer sur une retranscription écrite, qui peut ne pas être toujours pratique à utiliser dans certains contextes.</p>
<p>Pour jouer sur la vitesse de la vocalisation, cliquez sur NVDA dans les applications masquées (comme précédemment), puis <strong>Préférences</strong>, puis <strong>Paramètres</strong>, puis <strong>Parole</strong>.</p>
<figure><img src="https://www.itsonus.fr/blog_images/articles/guide_de_survie_lecteur_d_ecran/image-5.png" alt="Fenêtre de configuration de NVDA pour la configuration de la vitesse de vocalisation" loading="lazy" decoding="async"></figure>
<p>Une fois dans cette fenêtre, il ne reste plus qu’à jouer avec le slider correspondant à l’étiquette <strong>Débit</strong>.</p>
<h2 id="prendre-le-controle-de-la-vocalisation" tabindex="-1">Prendre le contrôle de la vocalisation</h2>
<p>Maintenant que vous êtes doté·e du bagage utile, il est temps de ne plus subir le lecteur d’écran.</p>
<p>Par défaut, le lecteur d’écran commencera à vocaliser dès que le contexte change. Par exemple lorsque vous passez d’un onglet à un autre dans votre navigateur, ou que vous changez d’application au premier plan, le lecteur d’écran restituera vocalement ce qui se passe. Cela a pour conséquence qu’il pourrait vous donner l’impression de ne jamais vous laisser en paix. C’est ici qu’interviennent les deux premières commandes à connaître.</p>
<h3 id="pauselecture" tabindex="-1">Pause/Lecture</h3>
<p>Pour mettre en pause une vocalisation en cours, un appui sur la touche <strong>Shift</strong> fera l’affaire. Cette touche agit comme un bouton pause d’une télécommande. Si vous rappuyez sur <strong>Shift</strong>, la vocalisation reprend exactement là où elle en était (même si elle était en plein milieu d’un mot).</p>
<h3 id="stop" tabindex="-1">Stop</h3>
<p>Pour interrompre définitivement une vocalisation en cours, il faudra presser la touche <strong>Ctrl</strong>. La différence avec la pause est qu’il est impossible de reprendre la vocalisation. Cette touche joue le rôle d’un stop. Il faudra ensuite utiliser les commandes de navigation pour relancer une vocalisation.</p>
<p>Jusqu’ici, on aura appris à pouvoir ne pas subir la vocalisation en étant capable de la démarrer ou l’interrompre. Mais il manque encore la capacité de pouvoir naviguer activement vers les éléments désirés.</p>
<h2 id="naviguer-avec-le-lecteur-d'ecran" tabindex="-1">Naviguer avec le lecteur d’écran</h2>
<p>Avec simplement quelques touches supplémentaires, il devient possible de pouvoir se déplacer à la demande dans la page, de façon linéaire (en parcourant les éléments séquentiellement, les uns après les autres).</p>
<p>Naturellement, il est possible de laisser le lecteur restituer toute la page, du début à la fin. Mais on se retrouve rapidement à manquer de finesse, surtout s’il faut systématiquement repartir du début de la page pour vocaliser un éléments spécifique qui se trouve en plein milieu du contenu. C’est ici qu’interviennent les quelques touches de navigation basique.</p>
<h3 id="touche-nvda" tabindex="-1">Touche NVDA</h3>
<p>Avant d’entrer dans le vif du sujet, il est nécessaire de connaître ce qu’on appelle les <strong>touches NVDA.</strong> Ces touches peuvent être configurées dans l’interface de NVDA dans <strong>Préférences</strong>, <strong>Paramètres, Clavier</strong>.</p>
<figure><img src="https://www.itsonus.fr/blog_images/articles/guide_de_survie_lecteur_d_ecran/image-4.png" alt="Fenêtre de paramètres pour configurer les touches NVDA du clavier" loading="lazy" decoding="async"></figure>
<p>Fenêtre de paramètres pour configurer les touches NVDA du clavier</p>
<p>Lors de l’appui sur une de ces touches, NVDA considère que vous appuyez sur une <strong>touche NVDA.</strong> Cela servira à exécuter certains raccourcis au clavier.</p>
<p>Passons maintenant aux commandes concrètes pour naviguer activement dans la page.</p>
<h3 id="lire-l'element-suivant" tabindex="-1">Lire l’élément suivant</h3>
<p>Un appui sur la touche <strong>flèche du bas</strong> interrompra la vocalisation en cours (s’il y en a une), et déplacera le focus NVDA vers le prochain élément de la page. Cet élément n’est pas nécessairement activable. Il peut s’agir d’un paragraphe, par exemple. Cela peut être pratique pour faire du pas à pas dans la vocalisation.</p>
<h3 id="lire-l'element-precedent" tabindex="-1">Lire l’élément précédent</h3>
<p>Un appui sur la touche <strong>flèche du haut</strong> fera l’inverse, à savoir déplacera le focus vers l’élément précédent dans la page</p>
<h3 id="redemarrer-la-lecture-de-la-page-a-partir-du-focus" tabindex="-1">Redémarrer la lecture de la page à partir du focus</h3>
<p>En combinant un appui sur une <strong>touche NVDA</strong> et la <strong>flèche du bas</strong>, la vocalisation reprend depuis l’élément qui a le focus. Il existe quelques subtilités par rapport à la touche pause/lecture <strong>Shift</strong>, mais ce n’est pas essentiel à connaître pour débuter. La vocalisation finira de restituer la page jusqu’à sa fin, et tant que vous ne l’interromprez pas.</p>
<h3 id="relire-l'element-courant" tabindex="-1">Relire l’élément courant</h3>
<p>En appuyant sur une <strong>touche NVDA</strong> et la <strong>flèche du haut</strong>, le lecteur d’écran vocalise à nouveau l’élément qui détient actuellement le focus. Cela permet notamment de lui faire répéter ce qui est focalisé.</p>
<h2 id="conclusion" tabindex="-1">Conclusion</h2>
<p>Avec ces quelques configurations et touches, il vous sera désormais possible de vous familiariser avec les bases de NVDA. Bien qu’il reste le défi de bien comprendre ce qui est vocalisé (et qui peut parfois apparaître quelque peu cryptique quand on débute), vous serez maintenant en capacité d’être le pilote de votre lecteur d’écran plutôt que de le subir.</p>
<p>Il existe bien d’autres commandes, mais celles-ci devraient pouvoir couvrir vos besoins essentiels en terme d’utilisation.</p>
<p>Si vous souhaitez aller plus loin, la <a href="https://www.nvda.fr/doc/userGuide.html">documentation officielle de NVDA</a> devrait répondre à toutes vos interrogations.</p>
";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:17:"
		
		
		
		
		
	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:71:"Quel rôle joue un architecte d'entreprise dans une démarche GreenIT ?";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"href";s:71:"https://www.itsonus.fr/blog/article/architecture-entreprise_et_greenit/";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2023-03-12T00:00:00Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:71:"https://www.itsonus.fr/blog/article/architecture-entreprise_et_greenit/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:7141:"<p><em>Pour y répondre, nous avons interviewé Damien Marzlin, membre du collectif IT’s on us. Damien est architecte d’entreprise, il accompagne les organisations dans la réduction des impacts environnementaux du numérique et forme à l’écoconception de services numériques.</em></p>
<h2 id="pour-commencer-peux-tu-nous-expliquer-ton-metier-d'architecte-d'entreprise" tabindex="-1">Pour commencer, peux-tu nous expliquer ton métier d’architecte d’entreprise ?</h2>
<p>Oui tout à fait. Une de mes premières missions est de cartographier l’existant. L’objectif est d’avoir connaissance de toutes les applications qui composent le système d’information et de connaître les fonctions de chaque application.</p>
<p>On sait donc, par exemple, qu’on a une application qui gère les commandes et qu’elle a pour fonction de réceptionner une commande web et d’envoyer des informations pour préparer la commande.</p>
<p>C’est une première vision assez statique, qu’on appelle POS (plan d’occupation des sols). A ce stade, il manque les liens entre les applications.</p>
<p>Dans la suite de mon exemple, l’étape suivante est de rajouter l’information me permettant de savoir comment je dois aller chercher les commandes web. Ce n’est pas parce qu’une application a la donnée que c’est à elle de les mettre à disposition.</p>
<p>Une bonne connaissance des applications et des liens entre elles, permet l’urbanisation. Et ainsi, couvrir un maximum de fonctions avec un minimum d’applications.</p>
<p>L’autre mission importante que j’ai, c’est d’avoir une vision long terme pour détecter les besoins que l’entreprise va avoir en termes de processus, d’applications et de techniques.</p>
<h2 id="peux-tu-nous-expliquer-comment-en-tant-qu'architecte-d'entreprise-tu-arrives-a-reduire-les-impacts-environnementaux-d'un-systeme-d'information" tabindex="-1">Peux-tu nous expliquer comment, en tant qu’architecte d’entreprise tu arrives à réduire les impacts environnementaux d’un système d’information ?</h2>
<p>Premièrement, il s’agit de ne pas recréer des fonctions existantes. Je constate assez souvent qu’une équipe projet qui ne connait pas l’architecture du système d’information va développer une nouvelle fonction en dehors du cadre d’architecture de l’entreprise.
Le premier lien évident, c’est qu’en ayant une bonne connaissance du système d’information, les architectes d’entreprises peuvent maximiser la réutilisation de l’existant. Au démarrage d’un projet, on liste les fonctions nécessaires et on identifie celles existantes et manquantes.</p>
<p>Ce qui peut aussi arriver, c’est que les flux de données entre les applications soient nombreux et non optimisés. Cela est dû au manque d’une vue logique des échanges entre les applications. On sait que le transfert de données a plus d’impacts environnementaux que le stockage. On comprend alors l’importance d’optimiser ces transferts.
Si on a une démarche d’uniformisation des flux, on va pouvoir extraire une seule fois la donnée, la mettre à disposition à un endroit et avoir plusieurs services qui utilisent l’information.
Par ailleurs, il arrive souvent que lorsqu’on met en place un flux, les équipes demandent une VM donc cela ajoute des impacts environnementaux.</p>
<h2 id="comment-cela-se-passe-concretement-en-entreprise" tabindex="-1">Comment cela se passe concrètement en entreprise ?</h2>
<p>Souvent, un ou plusieurs architectes sont dédiés à un domaine fonctionnel, selon sa taille et son importance.
Il y a des instances pour que les différents architectes de l’entreprise puissent communiquer. Dans ma mission actuelle, cette instance est hebdomadaire. Régulièrement, je constate qu’il y a des liens entre les domaines et qu’il est important de travailler en coopération.</p>
<p>Par exemple, l’un des domaines cherchait un outil de signature électronique, avant de chercher une solution, ils ont contacté les architectes d’entreprise pour savoir si quelqu’un avait déjà l’application, ce qui état le cas. Cette fonctionnalité a donc été réutilisée.</p>
<p>Je dois également sensibiliser les personnes qui prennent des décisions qui concerne le système d’information. Je participe donc aux instances où il faut prendre des décisions (Design Authority), pour essayer dès le début d’apporter une vision globale de l’existant et de ce qui peut être fait. Cela est vraiment efficace quand la décision est prise à la suite d’un dialogue entre l’architecte d’entreprise et sa hiérarchie.</p>
<h2 id="quels-sont-les-benefices-de-cette-approche" tabindex="-1">Quels sont les bénéfices de cette approche ?</h2>
<p>Il y a des gains économiques forts, car on peut mutualiser.</p>
<p>C’est plus intéressant de connecter 10 services à la même infrastructure que d’avoir 10 infrastructures différentes.
La mutualisation facilite le pilotage. Il y a qu’une seule équipe qui gère l’infrastructure et qui a une vision globale de l’existant pour déterminer ce qui peut être fait. C’est donc plus simple d’optimiser. Sans cela, il y a un risque élévé de surdimensionnement de plusieurs infrastructures.</p>
<p>Par ailleurs, l’uniformisation des flux de données, simplifie la maintenance. Plus il y a de flux, plus il est difficile de détecter des problèmes dans le transport de l’information et donc, de maintenir le système.</p>
<h2 id="peut-on-mesurer-les-gains-de-cette-demarche" tabindex="-1">Peut-on mesurer les gains de cette démarche ?</h2>
<p>L’architecture a un impact indirect, ça n’a pas de matérialité. Pour mesurer les bénéfices, il faut évaluer la matérialité grâce à un inventaire des équipements numériques. Dans les grandes entreprises il y a le CMDB (la base de données de gestion de configuraiton), une base de données qui contient tous les composants d’un système d’information de manière à avoir une vue d’ensemble sur leur organisation. On peut alors constater ce qu’on a évité (l’achat d’une machine virtuelle - VM - par exemple), le matériel qu’on a supprimé…</p>
<p>Si on veut avoir une idée des impacts environnementaux de façon précise, il faut réaliser une Analyse du Cycle de Vie (ACV) du système d’information. Il est ensuite possible de réaliser un plan d’action, de simuler les gains potentiels. Pour évaluer les bénéfices environnementaux réels, il faut refaire une ACV après la mise en oeuvre des actions.</p>
<h2 id="as-tu-un-conseil-pour-les-organisations-qui-n'ont-pas-encore-demarre-ce-chantier" tabindex="-1">As-tu un conseil pour les organisations qui n’ont pas encore démarré ce chantier ?</h2>
<p>La première chose à faire c’est de cartographier l’existant, cela permettra de prendre des décisions éclairées.
Je conseille également de prendre connaissances du référentiel Green IT : <a href="https://club.greenit.fr/doc/2022-06-GREENIT-Referentiel_maturite-v3.pdf">74 bonnes pratiques clés pour un numérique plus responsable</a> de GreenIT.fr.</p>
";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:17:"
		
		
		
		
		
	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:65:"Quelle est la place de l'écoconception dans le datawarehousing ?";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"href";s:69:"https://www.itsonus.fr/blog/article/ecoconception_et_datawarehousing/";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2023-01-09T00:00:00Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:69:"https://www.itsonus.fr/blog/article/ecoconception_et_datawarehousing/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:6204:"<p><em>Pour y répondre, nous avons interviewé Damien Marzlin, membre du collectif IT’s on us. Damien est architecte
d’entreprise, il accompagne les organisations dans la réduction des impacts environnementaux du numérique et forme à l’écoconception de services numériques.</em></p>
<h2 id="pour-commencer-damien-peux-tu-nous-expliquer-ce-qu'est-le-datawarehouse" tabindex="-1">Pour commencer Damien, peux-tu nous expliquer ce qu’est le datawarehouse ?</h2>
<p>C’est un entrepôt de données : un endroit où une entreprise recueille beaucoup de données qui viennent de différentes sources.
Ces données une fois collectées et mélangées, permettent d’en tirer une valeur analytique.</p>
<p>Les organisations peuvent être amenées à utiliser une datawarehouse pour les aider à prendre des décisions ou parce
qu’elles sont contraintes par un cadre légal de fournir des rapports.</p>
<p>Par exemple, chez Auchan, nous devons produire une déclaration de performance extra-financière (DPEF) , nous croisons
donc des données de ventes, RH, RSE, …</p>
<h2 id="quel-est-le-lien-entre-datawarehouse-et-ecoconception-de-services-numeriques" tabindex="-1">Quel est le lien entre datawarehouse et écoconception de services numériques ?</h2>
<p>Le datawarehousing est un service numérique composé de logiciels, d’équipements utilisateurs, de centre de données, d’équipements réseau. Bien souvent, il y a plusieurs applications, car plusieurs domaines métiers,
qu’il faut concevoir, développer et héberger.
Aussi, dans un datawarehouse il y a plusieurs étages : un premier pour la collecte, un deuxième où on normalise la
donnée, un troisième où on transforme la donnée en valeur et un dernier qui permet de visualiser.
Ces applications répondent a un ou plusieurs actes métiers qui doivent être écoconçus.</p>
<h2 id="a-quoi-faut-il-penser-pour-les-ecoconcevoir" tabindex="-1">À quoi faut-il penser pour les écoconcevoir ?</h2>
<h3 id="questionner-le-besoin" tabindex="-1">Questionner le besoin</h3>
<p>Le datawarehouse produit des reporting, des KPIs … qui vont être utilisés par des gens. Il est important de
questionner le besoin. Comme pour tous les projets informatiques, il faut donc s’assurer que ce qui est produit répond à
un réel besoin utilisateur.</p>
<p>Bien souvent, je constate lors dans mes missions, que ce qui est produit n’est pas utile pour les équipes. Soit parce que dès le départ le projet est mal cadré, soit parce que le projet a pris trop de temps et l’information n’est plus nécessaire. Parfois, on produit des rapports journaliers alors qu’un rapport hebdomadaire aurait suffi.</p>
<h3 id="correctement-requeter" tabindex="-1">Correctement requêter</h3>
<p>Avant il fallait réfléchir à la bonne configuration des serveurs, mais depuis qu’on a des environnements cloud ou du On-Premise on n’a plus de contrainte et donc on se pose moins de question. C’est ainsi plus facile, techniquement possible et économiquement viable, de manipuler des téraoctets de données. Ce qui a pour effet de consommer beaucoup de RAM (mémoire vive ou mémoire à court terme) et de CPU (capacité de calcul).</p>
<p>De manière générale, je constate que les bonnes sources d’information sont récupérées dans le datawarehouse.<br>
En revanche, régulièrement je vois des requêtes récupérant une grosse quantité de données pour ensuite les filtrer et/ou
les transformer en dehors de la requête : directement dans le code du programme sans utiliser le moteur de requêtage.
Ceci charge toutes les données et entraine donc une grosse montée en RAM. Le CPU est fortement utilisé également. Ces lourds traitements peuvent être évités en exploitant les capacités du serveur de base de données ; qui est prévu pour cela.</p>
<h2 id="comment-eviter-la-derive" tabindex="-1">Comment éviter la dérive ?</h2>
<p><strong>Un projet de datawarehousing, n’est pas qu’un projet technique, il faut intégrer dans l’équipe, et dès le début, une
personne responsable de l’expérience utilisateur.</strong> Son rôle sera de comprendre ce que vivent les utilisateurs (via des interviews, de l’observation par exemple) et d’identifier la problématique à laquelle il faut répondre. Se contenter de demander quel est le besoin des utilisateurs peut mener à un projet qui finalement ne sera pas utile.
Si le projet est terminé, ce qui a été produit lui doit continuer à vivre. Il faut donc penser à mettre en place des points de contrôle après la mise en production, pour s’assurer de l’utilité réelle des évolutions fonctionnelles. Vous pouvez vous appuyer sur les équipes support de l’utilisation du datawarehouse. Quels sont les échanges qu’ils ont avec les utilisateurs? Sont-ils satisfaits ou non ? Vous pouvez également aller sur le terrain pour échanger et observer l’usage qui est fait ou encore envoyer une enquête de satisfaction à intervalle régulier.</p>
<p>Un autre point clé est d’<strong>avoir un spécialiste en modélisation de données et en requêtage et de le faire intervenir
le plus tôt possible.</strong> C’est un savoir qui se perd, car cela attire moins les développeurs et développeuses. Il est
important d’entretenir cette compétence. Si ce profil n’existe pas dans votre équipe, rien que le fait de faire des
revues de code peut déjà apporter des bénéfices. Dans une équipe agile, on peut, par exemple, ajouter à la Definition Of
Done (DOD) un critère comme « les requêtes SQL ne font pas de produit cartésien » ou encore « la manipulation de données
parait nominale ». Finalement, on parle ici de qualité de code.</p>
<p>Si vous souhaitez commencer
une <a href="https://www.itsonus.fr/nos_services/reduire_impacts_numerique/">démarche de réduction des impacts environnementaux de votre projet de datawarehousing</a>,
découvrez notre offre de service et contactez-nous.</p>
<p>Vous pouvez également vous appuyer sur le <a href="https://club.greenit.fr/doc/2022-06-GREENIT-Referentiel_maturite-v3.pdf">référentiel Green IT : 74 bonnes pratiques clés pour un numérique plus responsable</a>.</p>
";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:17:"
		
		
		
		
		
	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:48:"Impacts environnementaux du numérique en France";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"href";s:74:"https://www.itsonus.fr/blog/article/impacts_envrionnementaux_du_numerique/";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2022-11-21T00:00:00Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:74:"https://www.itsonus.fr/blog/article/impacts_envrionnementaux_du_numerique/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:15771:"<p>Le numérique est au cœur de notre quotidien et bouleverse nos modes de vie et de travail. Il a également des effets
négatifs involontaires, entre autres sur notre environnement, qu’il faut absolument chercher à réduire car le numérique
contribue au dépassement de plusieurs limites planétaires, c’est-à-dire aux quantités d’impacts environnementaux
au-dessus desquels nous déstabiliseront les équilibres fondamentaux de la planète.</p>
<p>Rappelons que sur neuf limites planétaires, six ont désormais été dépassées : le changement climatique, l’érosion de la
biodiversité, les perturbations globales du cycle de l’azote et du phosphore, les changements d’utilisation des sols,
l’introduction de nouvelles substances (en 2022) et l’utilisation de l’eau douce (en 2022).</p>
<h2 id="principes-d'evaluation-environnementale" tabindex="-1">Principes d’évaluation environnementale</h2>
<p>Contrairement à ce que sous-entend l’univers sémantique du numérique (« cloud », dématérialisation, virtualisation…)
<strong>les services numériques ont une matérialité bien réelle</strong> : des équipements et des infrastructures sont nécessaires
pour
exécuter les logiciels, naviguer sur les sites web et utiliser les applications.</p>
<p>C’est bien cette matérialité qui engendre des impacts environnementaux. En effet, un logiciel n’a pas d’impacts
environnementaux à proprement parler (et ne s’écoconçoit pas) alors que les éléments physiques d’un service numérique en
ont.</p>
<p>Pour quantifier et analyser ces impacts, il est important de prendre en compte plusieurs critères environnementaux car
le numérique a bien plus d’effets négatifs que les seules émissions de gaz à effet de serre. Si l’analyse n’est pas
multicritère, le risque que les solutions apportées transfèrent les impacts d’un critère vers un autre est réel.</p>
<p>Il est également essentiel d’avoir une approche multi-étapes et systémique et de considérer l’ensemble des étapes du
cycle de vie de tous les éléments physique sous-jacents à une unité fonctionnelle. On définit l’objet d’une étude par la
fonction qu’il remplit afin de pouvoir comparer différentes solutions.</p>
<p>C’est là tout l’intérêt de <strong>la méthode holistique que représente l’analyse de cycle de vie (ACV)</strong>.
<a href="https://www.arcep.fr/uploads/tx_gspublication/etude-numerique-environnement-ademe-arcep-volet02_janv2022.pdf">Une évaluation environnementale</a>,
basée sur la méthode ACV, pilotée par l’ADEME et l’ARCEP et publiée en janvier 2022, quantifie et analyse les impacts
environnementaux du numérique en France au travers de l’unité fonctionnelle suivante :</p>
<p>« Utiliser les équipements et systèmes basés en France liés aux équipements et infrastructures numériques sur un an »</p>
<h2 id="materialite-des-equipements-et-infrastructures-numeriques-en-france" tabindex="-1">Matérialité des équipements et infrastructures numériques en France</h2>
<p>Selon cette étude, cette unité fonctionnelle couvre l’utilisation de plus d’un milliard d’équipements utilisateurs
(terminaux) et notamment :</p>
<ul>
<li>plus de 120 millions de téléphones, dont 70 millions de smartphones ;</li>
<li>près de 25 millions de tablettes ;</li>
<li>près de 60 millions d’ordinateurs portables et 40 millions d’ordinateurs fixes ;</li>
<li>environ 40 millions d’écrans d’ordinateur et 60 millions de téléviseurs ;</li>
<li>plus de 20 millions d’imprimantes ;</li>
<li>environ 245 millions d’objets connectés.</li>
</ul>
<p>Elle implique également des équipements réseau et des centres informatiques, entre autres :</p>
<ul>
<li>environ 30 millions d’abonnés (et donc de box internet) à des réseaux fixes ;</li>
<li>environs 95 millions d’abonnés à des réseaux mobiles, dont approximativement 20 millions pour des communications entre
machines ;</li>
<li>1,5 million de serveurs ;</li>
<li>15 millions de disques durs et SSD.</li>
</ul>
<p><strong>La matérialité du numérique est donc très fortement liée aux équipements des utilisateurs : il y a plus de 660
terminaux pour 1 serveur.</strong></p>
<p><strong>Nota bene :</strong> les impacts des équipements réseaux n’ont pas pu se baser sur un nombre d’équipements et leur durée de vie
car les opérateurs et équipementiers n’ont pas pu déterminer cette dernière. L’infrastructure réseau n’a pas été prise
en compte car des données ont aussi manqué à ce niveau. Cela fait partie des limites de cette étude.</p>
<h2 id="principaux-impacts-environnementaux-du-numerique-en-france" tabindex="-1">Principaux impacts environnementaux du numérique en France</h2>
<h3 id="impact-sur-le-changement-climatique" tabindex="-1">Impact sur le changement climatique</h3>
<p>Il se définit comme suit : « Les gaz à effet de serre (GES) sont des composés gazeux qui absorbent le rayonnement
infrarouge émis par la surface de la Terre. L’augmentation de leur concentration dans l’atmosphère terrestre contribue
au réchauffement climatique. »</p>
<p>Il représente 16,9 milliards de tonnes équivalent CO₂ soit les émissions d’un parc de plus de 12 millions de véhicules
parcourant plus de 12 000 km / an et émettant 112 g éq. CO₂ / km ou encore l’équivalent des émissions de plus de 2
millions d’habitants du monde.</p>
<h3 id="epuisement-des-ressources-abiotiques-elements-(minerais-metaux)" tabindex="-1">Épuisement des ressources abiotiques – éléments (minerais, métaux)</h3>
<p>La définition de cet impact est la suivante : « L’exploitation industrielle entraîne une diminution des ressources
disponibles dont les réserves sont limitées. Cet indicateur évalue la quantité de ressources minérales et métalliques
extraites de la nature comme s’il s’agissait d’antimoine ».</p>
<p>Le numérique en France contribue à l’épuisement des ressources abiotiques – éléments à hauteur de 948 tonnes équivalent
antimoine, soit près de 19 000 milliards de tonnes de terre excavée, ou encore, selon l’étude, l’équivalent de la
contribution de 15 millions d’habitants du monde.</p>
<h3 id="epuisement-des-ressources-abiotiques-fossiles" tabindex="-1">Épuisement des ressources abiotiques – fossiles</h3>
<p>« L’indicateur représente la consommation d’énergie primaire provenant de différentes sources non renouvelables
(pétrole, gaz naturel, etc.). Contrairement à ce que le nom indique, la consommation d’énergie primaire issue de
l’uranium est également considérée. Les calculs sont basés sur le Pouvoir Calorifique Inférieur (PCI) des types
d’énergie considérés, exprimé en MJ/kg. Par exemple, 1 kg de pétrole apporteront 41,87 MJ à l’indicateur considéré. »</p>
<p>Le numérique en France épuise ce type de ressource à hauteur de 796 milliards de mégajoules de ressources fossiles soit
la contribution à l’épuisement de ressources fossiles de 12 millions d’habitants du monde.</p>
<h3 id="radiations-ionisantes" tabindex="-1">Radiations ionisantes</h3>
<p>« Les radionucléides peuvent être libérés lors de plusieurs activités humaines. Lorsque les radionucléides se
désintègrent, ils libèrent des rayonnements ionisants. L’exposition humaine aux rayonnements ionisants provoque des
dommages à l’ADN, qui à leur tour peuvent conduire à divers types de cancer et de malformations congénitales. »</p>
<p>Le numérique en France contribue aux radiations ionisantes à la hauteur de 98 milliards de kilobecquerel équivalent
uranium 235 soit la radioactivité générée par les besoins de 25 millions d’habitants du monde.</p>
<h3 id="ecotoxicite-eaux-douces" tabindex="-1">Écotoxicité, eaux douces</h3>
<p>« Ces indicateurs suivent toute la chaîne d’impact depuis l’émission d’un composant chimique jusqu’à l’impact final sur
l’Homme et les écosystèmes. Cela comprend la modélisation de la distribution et du devenir dans l’environnement,
l’exposition des populations humaines et des écosystèmes, et les effets liés à la toxicité associés à l’exposition. »</p>
<p>La contribution du numérique en France à l’écotoxicité est de 263 milliards d’unités de toxicité pour des écosystèmes.
Cela correspond à l’écotoxicité générée par 6 millions d’habitants du monde.</p>
<h3 id="autres-impacts" tabindex="-1">Autres impacts</h3>
<p>Trois impacts environnementaux, parmi bien d’autres, méritent d’être soulignés. L’acidification, les émissions de
particules fines et la création d’ozone photochimique sont des effets négatifs significatifs du numérique : leur
quantité est équivalente à ceux générés par un million d’habitants du monde.</p>
<p><strong>Définitions :</strong></p>
<ul>
<li>
<p>Acidification :
« L’acidification de l’air est liée aux émissions d’oxydes d’azote, d’oxydes de soufre,
d’ammoniac et d’acide chlorhydrique. Ces polluants se transforment en acides en présence d’humidité, et leurs retombées
peuvent endommager les écosystèmes ainsi que les bâtiments. »</p>
</li>
<li>
<p>Émissions de particules fines :
« La présence de particules
fines de petit diamètre dans l’air - en particulier celles d’un diamètre inférieur à 10 microns - représente un problème
de santé humaine, car leur inhalation peut provoquer des problèmes respiratoires et cardiovasculaires. »</p>
</li>
<li>
<p>Création d’ozone photochimique :
« L’ozone troposphérique se forme dans la basse atmosphère à partir de composés organiques volatils
(COV) et d’oxydes d’azote résultant du rayonnement solaire. L’ozone est un oxydant très puissant connu pour avoir des
effets sur la santé, car il pénètre facilement dans les voies respiratoires. »</p>
</li>
</ul>
<p><strong>Nota bene :</strong> « L’indicateur d’épuisement de la ressource en eau [a donné] des résultats non cohérents du fait d’une
surreprésentation de la fin de vie liée aux données utilisées. Ce point a été identifié [par les auteurs], mais n’a pas
pu être corrigé dans le temps de l’étude. Soyons conscient que le numérique contribue aussi fortement au stress
hydrique. <a href="https://www.greenit.fr/impacts-environnementaux-du-numerique-en-france/">L’étude iNum : impacts environnementaux du numérique en France</a> de <a href="https://www.greenit.fr/">GreenIT.fr</a>, également basé sur une méthode ACV,
concluait que le numérique en France contribuait à la tension sur l’eau douce à hauteur de 559 millions de m³ d’eau
douce soit 5 fois la consommation d’eau des Parisiens. »</p>
<h3 id="impacts-du-numerique-en-france-par-habitant" tabindex="-1">Impacts du numérique en France par habitant</h3>
<p>L’impact annuel du numérique sur le changement climatique est de 235 kg éq. CO₂ par français, soit les émissions de gaz
à effet de serre équivalentes à celles d’un trajet de 2 259 km en voiture.</p>
<p>La production annuelle de déchets numériques est de 299 kg par habitant.</p>
<p>Pour répondre à ses besoins numériques un Français génère une masse de matériaux déplacée considérable de 932 kg chaque
année.</p>
<h2 id="source-des-impacts-du-numerique-en-france" tabindex="-1">Source des impacts du numérique en France</h2>
<p>Par leur nombre les équipements des utilisateurs représentent 63 à 92% de ces impacts!</p>
<p>À part pour les indicateurs épuisement des ressources – fossiles, radiations ionisantes et émissions de particules
fines, auxquels l’utilisation de ces équipements, et donc la production d’électricité pour les alimenter, contribue le
plus, <strong>c’est la fabrication des équipements des utilisateurs qui concentre la majorité des impacts</strong> et ce pour deux
raisons :</p>
<ul>
<li>ces équipements requièrent une quantité importante de métaux et de minerais, dont l’extraction demande beaucoup
d’énergie motrice – produite à partir de moteurs à explosion et d’hydrocarbures, de ressources et d’intrants
chimiques, et engendre beaucoup de déchets ;</li>
<li>la fabrication des équipements et infrastructures du numérique nécessite énormément d’énergie produite dans des pays
avec un mix énergétique très carboné ainsi qu’une grande force motrice générée par des moteurs à explosion.</li>
</ul>
<h3 id="poids-des-differents-equipements-dans-l'empreinte-environnementale-du-numerique-francais" tabindex="-1">Poids des différents équipements dans l’empreinte environnementale du numérique français</h3>
<p>Par leur nombre et leurs impacts unitaires importants, tant pour les fabriquer que pour les utiliser, les téléviseurs
représentent 11 à 30% des impacts du numérique français. Ils participent particulièrement à l’épuisement des ressources
– éléments.</p>
<p>Viennent ensuite les ordinateurs portables et fixes, les smartphones, les box TV, les consoles de jeu vidéo, les
imprimantes et les autres écrans qui par leur nombre et leurs impacts unitaires élevés expliquent 5 à 15% des impacts.</p>
<p>Les objets connectés, peu impactants unitairement, mais très nombreux, contribuent pour la majorité des impacts, de 3 à
6% de l’empreinte environnementale du numérique en France.</p>
<h3 id="reseau-fixe-vs-reseau-mobile" tabindex="-1">Réseau fixe VS réseau mobile</h3>
<p>Les impacts environnementaux des réseaux mobiles sont plus importants que ceux des réseaux fixes, par quantité de
données transférées.</p>
<h3 id="usages-personnels-vs-usages-professionnels" tabindex="-1">Usages personnels VS usages professionnels</h3>
<p>Les résultats de l’étude de l’ADEME et de l’ARCEP montrent que les usages professionnels contribuent de 37 à 48% des
impacts et les usages personnels contribuent de 42 à 63% des impacts.</p>
<h2 id="recommandations-et-reponses-de-it's-on-us" tabindex="-1">Recommandations et réponses de IT’s on us</h2>
<p>La fabrication est la principale source d’impacts pour l’environnement. Cela s’explique notamment par la quantité
importante d’énergies fossiles nécessaire à leur production et à l’extraction des minerais. L’utilisation représente
« seulement » 21 % de la contribution au changement climatique en raison du mix énergétique peu carboné de la France.</p>
<p>Les auteurs de l’étude recommandent donc d’allonger au maximum la durée de vie des équipements « à travers la durabilité
des produits, le réemploi, le reconditionnement, l’économie de la fonctionnalité ou la réparation ».</p>
<p>Les membres du collectif IT’s on us <a href="https://www.itsonus.fr/nos_services/reduire_impacts_numerique/">accompagnent les organisations</a> et les aident à :</p>
<ul>
<li>utiliser moins d’équipements numériques et à les faire durer plus longtemps grâce à la démarche Numérique Responsable.</li>
<li>écoconcevoir les services numériques en les dotant de couches applicatives fonctionnant sur de vieux équipements et</li>
<li><a href="https://www.itsonus.fr/nos_services/developper_modeles_durables/">changer de modèle économique</a>, quand elles appartiennent secteur du numérique, afin de sortir de la logique de volume
et corréler leurs intérêts économiques avec les intérêts socio-environnementaux.</li>
</ul>
<h2 id="sources" tabindex="-1">Sources</h2>
<ul>
<li><a href="https://www.arcep.fr/uploads/tx_gspublication/etude-numerique-environnement-ademe-arcep-volet02_janv2022.pdf">https://www.arcep.fr/uploads/tx_gspublication/etude-numerique-environnement-ademe-arcep-volet02_janv2022.pdf</a></li>
</ul>
";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:17:"
		
		
		
		
		
	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:91:"Benchmark 2023 : initier l'évaluation de l'empreinte environnementale de son organisation";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"href";s:59:"https://www.itsonus.fr/blog/article/benchmark_greenit_2023/";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2022-11-18T00:00:00Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:59:"https://www.itsonus.fr/blog/article/benchmark_greenit_2023/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:3379:"<p>À ce jour, le numérique a plus d’impacts environnementaux que l’aviation civile et ses impacts continuent d’augmenter.</p>
<p>Beaucoup d’entreprises initient un Bilan Carbone du Numérique. Il faut tout de même savoir que le gros des impacts du numérique se situent non pas sur l’émission de Gaz à Effet de Serre (11%), mais sur l’épuisement de ressources en métaux et minéraux (52%).</p>
<p>S’intéresser à seulement 11% du problème n’est pas suffisant.</p>
<p>Il est donc important de suivre la recommandation de la Commission Européenne d’utiliser la méthode d’analyse du Cycle de Vie. En prenant en compte la phase de fabrication, d’utilisation et de fin de vie, et 16 critères environnementaux (pour respecter le Product Environnemental Footprint), vous vous assurez de ne pas avoir de transfert d’impacts et de mettre en œuvre des actions pertinentes.</p>
<p><a href="https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32021H2279&amp;qid=1641580519072">Recommandation (UE) 2021/2279 de la Commission du 15 décembre 2021 relative à l’utilisation de méthodes d’empreinte environnementale pour mesurer et indiquer la performance environnementale des produits et des organisations sur l’ensemble du cycle de vie.</a></p>
<p>Afin d’aider les entreprises à initier cette mesure, chaque année depuis 2016, un benchmark green IT est réalisé.
En rejoignant ce benchmark, une organisation, quelque soit son secteur ou sa taille, peut évaluer son empreinte environnementale selon les standards ISO et le PEF.</p>
<p>Cela permet de :</p>
<ul>
<li>comprendre où se situent les impacts environnementaux de son système d’information</li>
<li>connaître sa maturité sur les 74 bonnes pratiques Green IT.</li>
<li>comparer son positionnement avec celui des aux organisations</li>
<li>identifier des actions pertinentes et prioritaires à mettre en œuvre</li>
<li>répondre à la pression réglementaire</li>
<li>échanger avec d’autres organisations ayant le même objectif de réduction de l’empreinte environnementale du numérique</li>
</ul>
<p>Les rapports sont accessibles sur le site du <a href="https://club.greenit.fr/">Club Green IT</a>.
Par exemple, le benchmark de 2021 était composé de 25 grandes entreprises privées et publiques, en France, Belgique, Luxembourg et Suisse, qui au global avaient :</p>
<ul>
<li>356 000 utilisateurs ;</li>
<li>1 000 000 d’équipements numériques (informatiques et télécoms) ;</li>
<li>9 900 m² DC (salle informatique).</li>
</ul>
<p>Le coût pour participer au Benchmark varie selon le chiffre d’affaires de l’entreprise de 2 000€ HT à 15 000€ HT.</p>
<p>Vous avez jusqu’à fin décembre pour rejoindre le Benchmark 2023.</p>
<p>Si vous souhaitez en savoir plus nous vous invitons à nous contacter : <a href="mailto:contact@itsonus.fr">contact@itsonus.fr</a> et à vous inscrire à <a href="https://www.eventbrite.fr/e/billets-benchmark-green-it-2022-et-2023-session-3-452610930477">la dernière session de présentation du Benchmark 2022 et lancement du Benchmark 2023</a></p>
<p>Si vous souhaitez réaliser un accompagnement individuel et des recommandations détaillées, nous vous invitons à découvrir notre offre de <a href="https://www.itsonus.fr/nos_services/reduire_impacts_numerique/">réduction des impacts environnementaux</a></p>
";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:17:"
		
		
		
		
		
	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:36:"Comment choisir un ERP responsable ?";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"href";s:68:"https://www.itsonus.fr/blog/article/comment_choisir_erp_responsable/";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2022-11-17T00:00:00Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:68:"https://www.itsonus.fr/blog/article/comment_choisir_erp_responsable/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:7776:"<p>Un ERP (Enterprise Resource Planning) est un progiciel de gestion intégré qui permet « de gérer l’ensemble des processus
d’une entreprise en intégrant l’ensemble de ses fonctions, dont la gestion des ressources humaines, la gestion
comptable et financière, l’aide à la décision, mais aussi la vente, la distribution, l’approvisionnement et le commerce
électronique » <a href="https://fr.wikipedia.org/wiki/Progiciel_de_gestion_int%C3%A9gr%C3%A9">Wikipedia</a></p>
<h2 id="les-entreprises-doivent-s'outiller-pour-suivre-l'evolution-de-la-reglementation" tabindex="-1">Les entreprises doivent s’outiller pour suivre l’évolution de la réglementation</h2>
<p><a href="https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000044044176">D’ici à 2026, toutes les entreprises vont devoir émettre des factures électroniques à transmettre aux clients
comme à l’administration fiscale.</a> Cela devrait permettre
de lutter contre les fraudes, notamment à la TVA.</p>
<p>La date d’application varie en fonction de la taille de la structure : juillet 2024 pour les grandes
entreprises, janvier 2025 pour les entreprises de tailles intermédiaires et janvier 2026 pour les PME et les
microentreprises.</p>
<p>Les entreprises doivent donc s’outiller pour rendre cela possible. C’est dans ce contexte que nous avons rencontré Equip’tout.</p>
<p>Equip’tout est une société familiale créée en 2000, devenue une référence dans le Nord de la France dans le domaine
de la <a href="http://www.equiptout.fr/index.php?id_cms=1&amp;controller=cms">location de matériels</a> pour les particuliers, les
professionnels et les collectivités.</p>
<p>Equip’tout défend des valeurs environnementales et sociales qui nous ont amenées à travailler ensemble pour répondre
aux nouvelles contraintes de dématérialisation de la facturation.</p>
<h2 id="la-demarche-pour-choisir-un-erp-responsable" tabindex="-1">La démarche pour choisir un ERP responsable</h2>
<p>Avant toute chose, il est important que chaque partie prenante du projet et notamment les décisionnaires aient une bonne <a href="https://www.itsonus.fr/enjeux_numerique/">compréhension des enjeux du numérique</a>. C’est pourquoi nous avons commencé notre mission par une sensibilisation.</p>
<p>Puis, comme toute démarche projet, il convient d’explorer par des interviews et de l’observation le geste métier
actuel. Cela nous a permis d’identifier les irritants et les points de satisfaction pour chercher la meilleure réponse.</p>
<p>Par exemple, nous avons appris qu’une partie du geste métier ne doit pas être numérisée pour s’assurer de la fluidité du
parcours. En effet, des fiches papiers sont utilisées dans le processus d’entretien des machines, ce qui donne de façon
rapide l’information du travail à réaliser sans manipuler un ordinateur.</p>
<figure><img src="https://www.itsonus.fr/blog_images/articles/comment_choisir_erp_responsable/image_1.jpg" alt="Fiches permettant de suivre l'entretien des machines" loading="lazy" decoding="async"><figcaption>Fiches permettant de suivre l’entretien des machines</figcaption></figure>
<p>Après avoir pris connaissance de l’existant, nous avons validé le périmètre du projet, identifié la parcours
utilisateur, validé les fonctionnalités prioritaires et réalisé une analyse d’architecture pour comprendre la gestion
des échanges et du stockage de données.</p>
<p>Nous avons ensuite réalisé une grille d’évaluation des outils selon différents critères : fonctionnels, techniques
et responsables.</p>
<p>Cette démarche a abouti à la réalisation d’un prototype permettant à Equip’tout de valider le choix de l’ERP,
ainsi que de créer de la confiance entre l’organisation retenue et Equip’tout.</p>
<h2 id="4-criteres-pour-choisir-un-erp-responsable" tabindex="-1">4 critères pour choisir un ERP responsable</h2>
<p>Au-delà de l’objectif d’avoir un ERP adapté au métier d’Equip’tout et conforme au futur changement de
réglementation, nous avions 4 objectifs complémentaires.</p>
<p><strong>Le service numérique est écoconçu</strong> afin de réduire les impacts environnementaux :</p>
<ul>
<li>le service fonctionne sur des vieux ordinateurs ;</li>
<li>les fonctionnalités sont utiles, utilisables et utilisées ;</li>
<li>les bonnes pratiques d’écoconception sont respectées. Vérifiez s’ils ont fait : une analyse du cycle de vie (ACV), une analyse de la maturité des 115 bonnes pratiques d’écoconception web ou une estimation des impacts avec EcoIndex.</li>
</ul>
<p><strong>L’ERP est accessible</strong> à tous quelque soit son contexte d’utilisation :</p>
<ul>
<li>le service numérique a une interface simple et une navigation possible au clavier ;</li>
<li>il possède une version web lisible par un lecteur d’écran ;</li>
<li>une démarche d’évaluation et d’amélioration est en cours. Par exemple, ils ont fait : un audit RGAA, ils sont certifiés OPQUAST et/ou formé à l’accessibilité numérique.</li>
</ul>
<p><strong>Le logiciel est open source,</strong> le service est accessible sans limites et le code est accessible, étudiable,
modifiable et redistribuable :</p>
<ul>
<li>il n’y a pas de surcoût de licence pour accéder à certaines fonctionnalités ;</li>
<li>une communauté existe ;</li>
<li>nous sommes libres de développer sur la plateforme avec n’importe quel partenaire.</li>
</ul>
<p><strong>La solution est durable</strong>, le service est modulable, il est possible de connecter / déconnecter des modules internes
et externes au service :</p>
<ul>
<li>le langage utilisé pour s’assurer que les connaissances sont présentes localement ;</li>
<li>la possibilité d’avoir une assistance et d’être formé à l’administration technique et métier ;</li>
<li>le service est exploitable au travers d’API.</li>
</ul>
<p>Cela n’a pas été le cas ici, mais il aurait été possible que certaines organisations soient labellisées, comme le Label
Numérique Responsable, le label B Corp ou face référence à des reconnaissances comme WattImpact. Cela ne doit pas
dispenser de questionner et de vérifier les véritables intentions des organisations.</p>
<h2 id="nous-avons-choisi-de-travailler-avec-tryton" tabindex="-1">Nous avons choisi de travailler avec Tryton</h2>
<p>Nous avons constaté que les ERPs répondent assez peu aux critères d’écoconception et d’accessibilité.</p>
<p>Cependant, notre choix s’est naturellement porté sur Tryton, pour plusieurs raisons :</p>
<ul>
<li>L’opportunité de concevoir de façon responsable le module de location non existant à date</li>
<li>La possibilité de naviguer au clavier et d’ajuster l’interface pour répondre au geste métier</li>
<li>L’absence de frais d’utilisation de l’ERP et la liberté de choisir les partenaires (véritablement open source)</li>
<li>La possibilité d’utiliser le service sans être connectée au réseau internet</li>
</ul>
<p>Cerdic Krier, manager chez B2CK qui est l’entreprise référente dans le déploiement de Tryton :
« Nous sommes ravis d’avoir été choisi pour faire les développements et les adaptations pour l’implémentation de
Tryton chez Equip’Tout. Cela va permettre d’étendre les fonctionnalités de base de Tryton avec un nouveau module
de gestion de location. »</p>
<p>Si vous souhaitez <a href="https://www.itsonus.fr/nos_services/reduire_impacts_numerique/">réduire l’empreinte environnementale et sociale de votre
solution</a> ou si vous souhaitez
vous <a href="https://www.itsonus.fr/nos_services/formations/">former au numérique responsable</a>, pensez à
nous <a href="https://www.itsonus.fr/contact/">contacter</a>.</p>
";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:17:"
		
		
		
		
		
	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:62:"Écoconception d'un site de contenu pour le Musée de Bretagne";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"href";s:74:"https://www.itsonus.fr/blog/article/ecoconception_web_exposition_celtique/";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2022-11-15T00:00:00Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:74:"https://www.itsonus.fr/blog/article/ecoconception_web_exposition_celtique/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:32516:"<p>En 2022, nous avons conçu et développé le site <a href="https://www.exposition-celtique.bzh">« Celtique ? L’expo »</a> dans une
démarche d’écoconception, afin d’accompagner l’exposition éponyme du Musée de Bretagne. L’écoconception étant un sujet
récent, nous avons dû expérimenter et faire certains arbitrages pour trouver les meilleures solutions. Nous souhaitons
partager ce retour d’expérience dans cet article et nous espérons que ce retour d’expérience pourra servir à :</p>
<ul>
<li>Illustrer ce qu’implique concrètement une démarche d’écoconception ;</li>
<li>Diffuser des solutions sobres répondant à certaines problématiques client ou utilisateur ;</li>
<li>Éviter à d’autres concepteurs de commettre les mêmes erreurs et de passer par les mêmes tâtonnements que nous dans leurs projets.</li>
</ul>
<h2 id="contexte-du-projet" tabindex="-1">Contexte du projet</h2>
<h3 id="objectifs-du-site" tabindex="-1">Objectifs du site</h3>
<p>Le <a href="https://www.musee-bretagne.fr/">Musée de Bretagne</a> a lancé du 18 mars au 4 décembre 2022 l’exposition « Celtique ? », qui interroge l’identité celte de la Bretagne contemporaine. Pour accompagner cette exposition physique, le Musée de Bretagne a souhaité lancer un site internet répondant à deux objectifs principaux :</p>
<ul>
<li>Rendre les contenus de l’exposition accessibles au plus grand nombre, notamment pour les personnes éloignées géographiquement ainsi qu’une fois l’exposition finie ;</li>
<li>Permettre aux visiteurs de poursuivre leur expérience en ligne via des ressources complémentaires.</li>
</ul>
<p>Le site reprend donc les contenus de l’exposition en les adaptant à un contexte web et en les enrichissant d’autres ressources disponibles en ligne.</p>
<h3 id="cahier-des-charges-du-site" tabindex="-1">Cahier des charges du site</h3>
<p>En cohérence avec ses valeurs de démocratisation de la culture et de rôle civique des institutions publiques, le cahier des charges émis imposait que le site soit :</p>
<ul>
<li>accessible (le plus possible)</li>
<li>éco-conçu (le plus sobre possible)</li>
<li>open-source (utilisant des solutions libres et partageant son code source)</li>
<li>cookie-free (ne collectant aucune donnée personnelle des utilisateurs).</li>
</ul>
<h3 id="equipe" tabindex="-1">Équipe</h3>
<p>L’équipe ayant réalisé ce projet était composée de :</p>
<ul>
<li><a href="https://www.linkedin.com/in/h%C3%A9l%C3%A8ne-ma%C3%AEtre-marchois-56758259/">Hélène Maître-Marchois</a> de <a href="https://fairness.coop/">Fairness</a>, interlocutrice commerciale</li>
<li><a href="https://www.linkedin.com/in/nicolas-doby-a4a96b96/">Nicolas Doby</a> de <a href="https://www.itsonus.fr/">IT’s on Us</a>, développeur</li>
<li><a href="https://www.linkedin.com/in/aureliebaton/">Aurélie Baton</a> et <a href="https://www.linkedin.com/in/anne-faubry-3b2b8390/">Anne Faubry</a>, UX/UI Designers</li>
</ul>
<p>Côté Musée de Bretagne, les interlocuteurs privilégiés avec lesquels l’équipe de développement a co-créé le site final étaient :</p>
<ul>
<li><a href="https://www.linkedin.com/in/manuel-moreau-44b66214a/">Manuel Moreau</a>, Prospective et Innovation</li>
<li><a href="https://www.linkedin.com/in/sarah-lemiale/">Sarah Lemiale</a>, Cheffe de projet d’exposition</li>
</ul>
<h3 id="resultat" tabindex="-1">Résultat</h3>
<p>La conception et le développement du site se sont déroulés de fin novembre 2021 à mars 2022. Le site a été publié le 13 juin 2022.</p>
<p><strong>Les 15 pages de contenu du site sont toutes notées A ou B par ecoindex.</strong> Le nombre moyen de requêtes est de 15, <strong>le poids
moyen des pages est de 658 Ko</strong> et la taille moyenne du DOM est de 310. Le site se charge en 2,5 secondes avec une connexion
3G moyenne. L’empreinte environnementale du parcours principal équivaut à 10 g équivalent CO₂ de gaz à effet de serre
et 15 cl d’eau selon les mesures effectuées avec GreenIT Analysis.</p>
<p>Retrouvez le détail des mesures d’impacts environnementaux effectuées et leurs explications sur la page <a href="https://www.exposition-celtique.bzh/ecoconception/">Écoconception</a>.</p>
<p>L’écoconception de services numériques a permis de minimiser les impacts environnementaux du service web tout en
améliorant la performance d’usage pour le visiteur du site.</p>
<h2 id="problematiques-rencontrees" tabindex="-1">Problématiques rencontrées</h2>
<p>Cet article vise avant tout à partager les problématiques de conception rencontrées ainsi que les solutions adoptées.</p>
<h3 id="navigation" tabindex="-1">Navigation</h3>
<p><strong>En écoconception de services numériques :</strong> L’un des ressorts principaux de l’écoconception consiste à fluidifier au maximum le parcours
utilisateur. Une navigation complexe entraîne des allers-retours et des chargements de pages superflus.</p>
<p><strong>Problèmes rencontrés :</strong> L’arborescence initialement prévue correspondait à l’architecture de l’exposition et au
parcours physique des visiteurs. Or, elle était beaucoup trop complexe pour une architecture de site web. Les tests menés
nous ont en effet révélé que les utilisateurs ne parvenaient pas à savoir où ils se trouvaient sur le site ou à retrouver
une page sur laquelle ils étaient allés auparavant.</p>
<p>Par ailleurs le cahier des charges indiquait que le site devait répondre à deux usages de navigation distincts :</p>
<ul>
<li>Navigation linéaire : lecture des contenus dans l’ordre chronologique, celui des chapitres</li>
<li>Navigation transverse : lecture des contenus selon les sujets abordés, en sautant des parties, dans le désordre</li>
</ul>
<p><strong>Solutions :</strong> Les 4 niveaux de profondeur de l’arborescence ont été réduits à 3. Les longueurs de chaque partie ont été harmonisées afin de se retrouver avec 2 parties de 3 et 5 chapitres au lieu de 4 parties de 1 à 4 chapitres. Cela a nécessité des réécritures et des coupes dans le texte, choisies avec le Musée de Bretagne.
Deux palettes de couleurs distinctes ont également été retenues pour aider les utilisateurs à identifier où ils se trouvaient.
Dans les parties les plus riches, nous avons ajouté un sommaire sous l’introduction et un chemin de fer cliquable en haut de page, permettant des navigations transverses selon les centres d’intérêt de l’internaute. Des boutons « précédent » et « suivant » ont également été travaillés pour les usages de navigation plus linéaires.</p>
<figure><img src="https://www.itsonus.fr/blog_images/articles/ecoconception_web_exposition_celtique/image_1.jpg" alt="Différentes navigations : chemin de fer cliquable, boutons précédent/suivant" loading="lazy" decoding="async"><figcaption>Différentes navigations : chemin de fer cliquable, boutons précédent/suivant</figcaption></figure>
<p>Ces modes de navigation devaient être également accessibles sur mobile. Le chemin de fer a dû être simplifié.</p>
<p><strong>Nota bene :</strong> Certains utilisateurs, notamment les moins à l’aise avec le numérique, ne pensent pas nécessairement à cliquer sur le logo pour revenir à l’accueil. Il est important de prévoir une entrée « Accueil » dans le menu pour ces personnes.</p>
<h3 id="photos" tabindex="-1">Photos</h3>
<p><strong>En écoconception de services numériques :</strong> En 2022, les images d’une page web représentent en moyenne plus
de 1 Mo (<a href="https://httparchive.org/reports/page-weight">HTTP Archive</a>). Or GreenIT.fr recommande de rester sous
la barre de 1 Mo pour le poids d’une page.</p>
<p><strong>Problèmes rencontrés :</strong> Le site “Celtique ?” est un site de contenu, présentant des photos de collections de
musées. Plusieurs pages intègrent une dizaine d’images de bonne qualité. Certains utilisateurs, comme des enseignants
et des chercheurs, peuvent avoir besoin d’accéder aux images en haute définition. Certaines photos des œuvres transmises
étaient d’excellente qualité à des fins de conservation, et pesaient à l’origine plus de 50 Mo.</p>
<p><strong>Solutions :</strong> Comme toujours en écoconception, il s’agit tout d’abord d’interroger le besoin. Ici, il est
double : accompagner les textes explicatifs et s’immerger dans les collections. Certaines images superflues ont ainsi
été écartées pour réduire leur nombre à 10 par page de contenu au maximum.</p>
<p>Deuxièmement, nous nous sommes interrogés sur la définition nécessaire pour les images. Nous avons choisi de les
afficher en taille moyenne pour la plupart (entre 300 et 800 pixels de large) avec une <strong>option « Voir en résolution
maximale »</strong> pour accéder à la haute définition.</p>
<figure><img src="https://www.itsonus.fr/blog_images/articles/ecoconception_web_exposition_celtique/image_2.jpg" alt="Figure suivi d'un lien pour voir la version haute résolution" loading="lazy" decoding="async"><figcaption>Figure suivi d’un lien pour voir la version haute résolution</figcaption></figure>
<p>Les photos ont été fortement optimisées. Ainsi, même en haute définition, les images ne font pas plus de 1024 pixels
de large et conservent un poids inférieur à 2 Mo. Les images de taille moyenne font quant à elles généralement
entre 50 et 200 Ko.</p>
<p>Pour accéder à notre méthode de compression des
images voir : <a href="https://www.exposition-celtique.bzh/ecoconception/">https://www.exposition-celtique.bzh/ecoconception/</a>.</p>
<p>Pour les images vectorielles, nous avons privilégié le format SVG qui est généralement plus léger que le JPEG,
sauf pour les images vectorielles complexes telles que la « Carte des flux de populations et échanges économiques et
culturels au haut Moyen  ge » qui était plus lourde au format SVG.</p>
<p>Lorsque possible, certaines images vectorielles ont été remplacées par des glyphes : par exemple la flèche
précédent / suivant est un caractère.</p>
<h3 id="videos" tabindex="-1">Vidéos</h3>
<p><strong>En écoconception de services numériques :</strong> Les vidéos représentent environ 80% des données transférées sur
internet. Gourmand en énergie, le streaming a des impacts environnementaux considérables : obsolescence accélérée du
matériel, renouvellement des infrastructures générant un effet rebond, exigences élevées en stockage, transfert de
données… Il s’agit de les réduire au maximum.</p>
<p><strong>Problèmes rencontrés :</strong> La vidéo est un média populaire, efficace en communication. Les expositions sont souvent présentées par une vidéo, généralement positionnées haut sur la page d’accueil pour inciter au visionnage. Plusieurs interviews et vidéos explicatives font également partie de l’exposition, pesant 200 Mo à 500 Mo chacune. Par ailleurs, le plugin Youtube, souvent utilisé pour incruster les vidéos dans la page pèse à lui seul 2,5 Mo et nécessite de nombreuses requêtes.</p>
<p><strong>Solutions :</strong> Certaines vidéos ont été placées plus bas sur les pages, voire en lien externe vers Youtube pour ne pas trop inciter à les visionner lorsqu’elles n’étaient pas essentielles à la compréhension.</p>
<p>Elles ont été <strong>compressées à 720p et fortement optimisées</strong>, permettant souvent de diviser leur poids par 15. Pour accéder à
notre méthode de compression des images voir : <a href="https://www.exposition-celtique.bzh/ecoconception/">https://www.exposition-celtique.bzh/ecoconception/</a>.</p>
<p>Les vidéos ont été incrustées en HTML5. Cela permet de ne pas avoir à télécharger un nouveau lecteur réduisant fortement le nombre de requêtes.</p>
<h3 id="graphisme" tabindex="-1">Graphisme</h3>
<p><strong>En écoconception de services numériques :</strong> Les chartes graphiques des organisations sont rarement pensées pour le
web, et encore moins pour la sobriété et l’accessibilité. Elles présentent souvent des logos non adaptés au mobile, des
couleurs au contraste insuffisant, des typographies personnalisées aux nombreuses variations qui viennent alourdir
le site…
Le préjugé selon lequel un site appliquant une démarche d’écoconception sera laid ou austère a encore la peau dure. Il
existe pourtant différentes astuces (<a href="https://eco-conception.designersethiques.org/guide/fr/content/10-ressources.html">liste d’exemples disponible ici</a>) pour créer un site avenant et différenciant tout
en étant sobre.</p>
<p><strong>Problèmes rencontrés :</strong> Le site Celtique ? devait être immersif, donner envie de lire le contenu, mettre en
avant les collections, mais sans alourdir inutilement les pages. Les éléments de charte partagés, conçus pour
l’exposition physique, s’adaptaient mal à un contexte web (problèmes de lisibilité, poids des médias…).</p>
<p><strong>Solutions :</strong> Il existe plusieurs recours en écoconception pour apporter du dynamisme et de l’identité
sans surpoids : utiliser la couleur, recourir à des formes SVG, créer des glyphes, faire varier les formes.</p>
<ul>
<li>Ainsi nous avons repris les couleurs marquées de l’exposition (turquoise, orange, rose vif…) en les modifiant parfois légèrement pour s’assurer que les contrastes étaient accessibles.</li>
<li>Nous avons utilisé les triskells créés pour l’exposition en filigrane : en format SVG, ils ne pesaient chacun que 2 à 5 Ko.</li>
<li>Nous avons créé de nouvelles formes SVG pour casser le côté austère et apporter de la fantaisie : des vagues, des blobs (voir forme orange ci-dessous)…</li>
<li>Le tiret orange en vague devant le titre a été intégré en glyphe (voir ci-dessous).</li>
</ul>
<figure><img src="https://www.itsonus.fr/blog_images/articles/ecoconception_web_exposition_celtique/image-6.jpg" alt="Illustration du tiret orange devant un titre, et de différentes formes" loading="lazy" decoding="async"></figure>
<ul>
<li>Enfin la feuille de styles (CSS) permet de faire de nombreuses variations sans recourir à des surcouches. Par
exemple jouer sur les arrondis, les contours, les ombres…(voir exemple du sommaire ci-dessous)</li>
</ul>
<figure><img src="https://www.itsonus.fr/blog_images/articles/ecoconception_web_exposition_celtique/image-7.jpg" alt="Sommaire montrant des arrondis réalisés en CSS" loading="lazy" decoding="async"></figure>
<h3 id="police" tabindex="-1">Police</h3>
<p><strong>En écoconception de services numériques :</strong> Même si les polices ne sont pas l’élément le plus impactant, elles contribuent à accroître
le poids de la page et les transferts nécessaires à leur téléchargement. En utilisant des polices
standards, pré-installées sur l’ordinateur on économise donc de la bande passante tout en accélérant l’affichage de la page.</p>
<p><strong>Problèmes rencontrés :</strong> Comme de nombreux évènement culturels, l’exposition « Celtique ? » a une forte identité
et la typographie choisie pour l’exposition physique est très marquée et rappelle le caractère celtique. La typographie
créée pour l’exposition « Celtique ? » n’était pas accessible et alourdissait le site (93 Ko par variation de police qui
seront téléchargés par tous les utilisateurs lors de leur première visite).</p>
<p><strong>Solutions :</strong> Pour répondre à l’objectif d’écoconception tout en gardant l’identité de l’exposition, nous avons utilisé la
police <strong>Arial</strong> pour le contenu des pages (titres, sous-titres, texte), mais nous avons choisi une <strong>image SVG</strong> pour conserver
l’apparence de la typographie dans le titre de l’exposition.</p>
<figure><img src="https://www.itsonus.fr/blog_images/articles/ecoconception_web_exposition_celtique/image_3.jpg" alt="Titre du site utilisant une typographie celtique" loading="lazy" decoding="async"><figcaption>Titre du site utilisant une typographie « celtique »</figcaption></figure>
<h3 id="interactions" tabindex="-1">Interactions</h3>
<p><strong>En écoconception de services numériques :</strong> Certaines interactions et animations peuvent être coûteuses en termes de ressources car elles
nécessitent dans certains cas l’implémentation de JavaScript ou de certaines propriétés CSS à éviter.</p>
<p><strong>Problèmes rencontrés :</strong>
Pour rendre l’exposition interactive et accessible au plus grand nombre, le Musée de Bretagne avait prévu un test (quiz) pour que chacun puisse découvrir son “profil celte”. L’idée était donc de conserver ce questionnaire pour la version en ligne de l’exposition. Mais comment rendre un test suffisamment interactif tout en limitant l’utilisation de ressources ?</p>
<p>Les questions du questionnaire étaient au départ au nombre de 12. Nous avions envisagé une barre de progression afin que l’utilisateur ait une indication visuelle de sa progression dans le questionnaire.</p>
<p><strong>Solutions :</strong>
Après plusieurs tests utilisateurs sur les maquettes il est finalement devenu apparent qu’il y avait trop de questions. Nous
avons donc proposé de passer à 9 questions au lieu de 12. Les tests utilisateurs ont été concluants et la barre de
progression était finalement superflue. Nous avons finalement décidé d’éliminer la barre de progression ce qui
nécessitait également moins de développement.</p>
<p>Cependant, afin que l’utilisateur ait un retour clair avant validation, nous avons conservé un <strong>simple décompte
des réponses</strong> en utilisant un simple code Javascript.</p>
<figure><img src="https://www.itsonus.fr/blog_images/articles/ecoconception_web_exposition_celtique/image_4.jpg" alt="Décompte des réponses restantes" loading="lazy" decoding="async"><figcaption>Décompte des réponses restantes</figcaption></figure>
<h3 id="page-ecoconception" tabindex="-1">Page Écoconception</h3>
<p><strong>En écoconception de services numériques :</strong> Une des bonnes pratiques de l’écoconception consiste à documenter la démarche pour faire
avancer le sujet et partager l’expérience, mais aussi afin d’expliquer le contexte et les limites du projet.</p>
<p><strong>Problèmes rencontrés :</strong> Comme de nombreux projets entamant une démarche d’écoconception, il y a d’un côté,
l’objectif à atteindre, et de l’autre, les limites auxquelles nous sommes confrontés. Nous avons pu mettre
en place une grande partie des bonnes pratiques d’écoconception, mais certaines n’ont pas pu être mises en place
pour le moment car elles touchent à l’écosystème plus global des Champs Libres (par exemple l’hébergement des
vidéos sur une plateforme de type PeerTube ou les statistiques de fréquentation du site sur un outil tel que Matomo).</p>
<p><strong>Solutions :</strong> Plutôt que d’afficher un score EcoIndex en bas de page ou bien une estimation de l’empreinte
environnementale, nous avons préféré (après discussion avec l’équipe du musée) documenter notre <strong>intention
d’écoconception dans une page</strong> disponible dans le pied de page du site, au même titre que la déclaration d’accessibilité.</p>
<figure><img src="https://www.itsonus.fr/blog_images/articles/ecoconception_web_exposition_celtique/image_5.jpg" alt="Bas de page contenant le lien vers la page écoconception" loading="lazy" decoding="async"><figcaption>Bas de page contenant le lien vers la page écoconception</figcaption></figure>
<h3 id="techniques" tabindex="-1">Techniques</h3>
<p>La particularité de ce projet est qu’il ne nécessite aucune modification graphique, aucun ajout de page ou de contenu. Seules des corrections textuelles mineures seront réalisées sur ce site. De ce fait, un générateur de site statique répond amplement au besoin. Ceci permettant de générer une seule fois les ressources statiques (HTML, CSS, JS) lors de l’installation contrairement à des sites dynamiques qui génèrent les pages à la volée, à chaque visite du site web. Ce choix soulage la charge serveur, le transformant en simple fournisseur de fichiers et non un calculateur récurrent.</p>
<p>Le générateur de <a href="https://www.11ty.dev/">sites statiques 11ty</a> a été retenu en utilisant pour la plupart des pages un format Markdown et un template global pour la mise en forme. Le format Markdown, étant lisible sans avoir de connaissance technique, il est aisé d’apporter des corrections sur le fond sans “casser” la forme.</p>
<p>Au fur et à mesure des avancées des maquettes, il s’est avéré que chacune des pages étaient différentes. Le Markdown perdant son avantage d’être lisible, il a été décidé de passer sur un format Nunjunk ; permettant d’avoir plus de souplesse dans le développement.</p>
<p>Ce changement a induit de former les équipes non-techniques sur ces aspects techniques :</p>
<ul>
<li>l’architecture technique du projet : savoir à quel endroit se trouve les textes à modifier,</li>
<li>comment modifier des fichiers sources Nunjunk en respectant un format contraignant.</li>
</ul>
<p>Une fois les modifications réalisées, les équipes techniques peuvent générer et re-déployer une version de production.</p>
<h2 id="le-retour-d'experience-du-musee-de-bretagne" tabindex="-1">Le retour d’expérience du musée de Bretagne</h2>
<p>L’idée de développer un site web en complément d’une exposition temporaire n’est pas nouvelle pour le Musée de Bretagne
aux Champs Libres. Plusieurs expositions présentées depuis 2006 ont donné lieu à la création de mini-sites dédiés, sans
compter les projets menés avec des partenaires. L’objectif d’alors est double : promotionnel et documentaire. La mise en
ligne du portail des collections du musée en 2017 a également permis d’affirmer l’engagement du musée vers une mise à
disposition croissante de ressources culturelles en ligne, et cela avec une démarche volontariste d’ouverture des
contenus, dans la philosophie de l’open content portée notamment par Wikimédia. <strong>Considérant le développement d’un site
portail des Champs Libres et la mise en ligne de parcours thématiques sur le portail des collections, le musée de
Bretagne ne s’était plus engagé dans la création d’un véritable site dédié depuis 2012.</strong></p>
<h3 id="les-motivations-du-projet" tabindex="-1">Les motivations du projet</h3>
<p><i>Pourquoi avoir choisi de s’engager dans un nouveau projet de site web dédié à l’exposition Celtique ?<a href="https://www.itsonus.fr/blog/article/ecoconception_web_exposition_celtique/">[1]</a> avec une
ambition écoresponsable ?</i></p>
<p>Le contexte « global » d’abord. <strong>La crise sanitaire et ses conséquences sur les établissements culturels (fermeture,
ouverture partielle…) ont amené ceux-ci à se poser la question du renforcement de leur présence en ligne</strong>, pour « garder
le lien » avec leur public. Le musée de Bretagne souhaitait aussi expérimenter des liens entre le in situ et le online,
le visiteur de l’exposition pouvant ouvrir des fenêtres « numériques » au cours de sa visite. Néanmoins, force est de
constater la difficulté à obtenir des données stabilisées sur les véritables bénéfices/coûts (temps investi, notoriété,
impacts sur la fréquentation insitu) de tels projets pour les établissements concernés.</p>
<p>La seconde motivation est liée à la <strong>stratégie globale de l’établissement</strong>, celle des Champs Libres comme celle du Musée
de Bretagne. S’inscrivant dans la dynamique de la mise en ligne du portail des collections en 2017, l’amplification des
projets de diffusion numérique reste un enjeu fort porté par le musée<a href="https://www.itsonus.fr/blog/article/ecoconception_web_exposition_celtique/">[2]</a>. Dans le même temps, la réflexion en cours aux
Champs Libres vers un nouvel écosystème web, avec comme orientation principale <strong>« une stratégie pour un numérique
soutenable »</strong>, prend de l’ampleur. Le projet web Celtique ? s’oriente alors vers une ambition d’écoresponsabilité plus
marquée. L’idée est d’avoir un projet pilote sur lequel le musée peut s’appuyer, voire réutiliser des briques, pour des
projets à venir. Pour autant, il ne s’agit pas de systématiser, voire d’industrialiser, la production de site à chaque
exposition. L’équipe du musée convient que l’opportunité d’une production web doit être questionnée à chaque fois au
même titre que les autres composantes du projet : médiation, programmation, édition…</p>
<p><strong>Le Musée de Bretagne s’engage donc dans ce projet avec l’approche d’une démarche quasi-innovante car encore peu
expérimentée et documentée jusqu’alors.</strong></p>
<p><span id="ref-1">[1] Présentée au musée de Bretagne du 17 mars au 4 décembre 2022</span><br>
<span id="ref-2">[2] Inscrit dans son projet scientifique et culturel depuis 2015</span></p>
<h3 id="une-organisation-dediee" tabindex="-1">Une organisation dédiée</h3>
<p>Pour répondre aux enjeux précisés en amont, le choix retenu a été celui d’un <strong>co-pilotage interne innovation/production</strong>. L’expertise web du musée se situe du côté du chargé de mission prospective et innovation, qui se trouve également
être le référent musée au sein du groupe projet écosystème web des Champs Libres. Côté pôle production, la cellule
production des expositions se voit légitimement attribuer le co-pilotage du projet (via la cheffe de projet de
l’exposition, puis la chargée de productions audiovisuelles), par sa capacité à faire le lien avec les contenus et à
organiser la production en cohérence, de calendrier et de ressources, avec les autres composantes du projet
d’exposition.</p>
<h3 id="l'adaptation-continue-du-projet" tabindex="-1">L’adaptation continue du projet</h3>
<p>Le projet web Celtique ? s’inscrit, dès son amorce, dans un plan de charge contraint. Ayant à l’esprit la règle du
« moyen constant », le projet va progressivement évoluer dans ses intentions. D’un projet qui devait permettre l’accès à
des ressources complémentaires, voire inédites (qui demande un surcroît de production et de coordination éditoriale), il
devient un espace « relai » de l’exposition où l’on retrouve les ressources présentées dans une organisation plus propice
à la consultation en ligne en amont ou à la sortie de sa visite.</p>
<p><strong>L’enjeux d’un site le plus écoresponsable possible dans un budget contraint a également amené l’équipe du musée à revoir
certaines orientations liées à l’aspect interactif et l’attractivité visuelle du site, ainsi que sur l’utilisation
raisonnée des médias.</strong> L’un des enseignements retenus par les équipes du musée est alors de considérer qu’un site plus
écoresponsable n’en n’est pas moins coûteux qu’un site classique, voire un peu plus. Mais il s’agit ici de penser à
plus long terme : un projet moins coûteux pour l’environnement, avec des briques réutilisables qui en assurent une
certaine reproductibilité…</p>
<p>Ces adaptations ont pu être intégrées de manière assez fluide grâce à <strong>l’agilité et la régularité des échanges entre le
prestataire et les équipes du musée, fonctionnant sur le principe de l’itération</strong>. Une méthodologie nécessaire dans le
cadre de tel projet.</p>
<h3 id="quelques-questions-specifiques-soulevees-au-cours-de-la-mise-en-oeuvre-de-ce-projet" tabindex="-1">Quelques questions spécifiques soulevées au cours de la mise en œuvre de ce projet</h3>
<h4 id="sur-quelle-base-fixe-t-on-le-niveau-d'ecoresponsabilite" tabindex="-1">Sur quelle base fixe-t-on le niveau d’écoresponsabilité ?</h4>
<p>Pour s’engager dans un projet écoresponsable, il importe de se fixer des objectifs chiffrés, et pour <strong>cela il faut des
indicateurs précis à atteindre</strong>. Aujourd’hui, ces données sont encore difficiles à trouver. Pour le musée de Bretagne,
s’engager dans le projet web Celtique ? visait donc aussi à capitaliser sur un retour d’expérience, et agréger des
données possiblement réutilisables sur des projets futurs, tout en s’appuyant sur l’expertise d’un prestataire
expérimenté et soucieux de partager ces connaissances notamment
ici : <a href="https://www.exposition-celtique.bzh/ecoconception/">https://www.exposition-celtique.bzh/ecoconception/</a></p>
<h4 id="a-qui-destine-t-on-cette-offre" tabindex="-1">À qui destine-t-on cette offre ?</h4>
<p>Répondre clairement à cette question avant de s’engager dans le projet permet de mieux définir les critères d’arbitrage
en cours de réalisation. <strong>Un site écoresponsable est surtout un site qui répond à ses objectifs initiaux, sans éléments
superficiels ou autre effet « waouh! »</strong>. Dans le cas présent, le site web remplit sa mission d’ouverture et de liens vers
des contenus complémentaires sans toutefois proposer sur le site beaucoup plus de ressources que celles de l’exposition.
Il est à destination de personnes qui ne pourraient peut-être pas visiter l’exposition mais souhaitent pouvoir
approfondir le sujet, sans pour autant devenir expert.</p>
<h4 id="a-quel-niveau-peut-on-se-connecter-aux-ressources-presentes-chez-nos-partenaires" tabindex="-1">À quel niveau peut-on se connecter aux ressources présentes chez nos partenaires ?</h4>
<p>Proposer un écosystème documentaire enrichi est certainement l’ambition de la plupart des sites web d’exposition. Pour
autant, la fluidité du parcours du visiteur ne doit jamais être affectée par le souhait d’une trop grande serendipité
des contenus. En somme, le visiteur doit pouvoir flâner sans jamais se perdre. Les cheminements entre les pages ont été
mis en évidence et testés pour viser cet objectif (voir la partie <a href="https://www.itsonus.fr/blog/article/ecoconception_web_exposition_celtique/">Navigation</a> plus haut). <strong>De même, la valeur
écoresponsable d’un site peut être réduite si celui-ci propose des renvois trop systématiques vers des univers web
éloignés de ces enjeux.</strong> C’est ainsi que le musée de Bretagne, en s’associant à Bécédia, a choisi un partenaire culturel
sensible à ces enjeux.</p>
<h3 id="perspectives" tabindex="-1">Perspectives</h3>
<p>L’ensemble des enseignements recueillis lors de ce projet, tant du point de vue technique que méthodologique, vient
aujourd’hui nourrir les évolutions en cours de notre écosystème web.</p>
<p>L’un des prochains chantiers est celui de la refonte de notre portail des collections en ligne. Le cahier des charges en
cours de rédaction s’inspire déjà en partie des leçons retenues pour viser le maximum d’écoresponsabilité. L’enjeu est
élevé puisqu’il concerne une base de données qui comporte aujourd’hui 350 000 notices et images associées, avec le souci
d’équilibrer le projet entre sobriété du design et qualité des contenus diffusés.</p>
<h2 id="notre-facon-de-collaborer" tabindex="-1">Notre façon de collaborer</h2>
<p>Tout au long du projet, nous avons travaillé de manière itérative et régulière entre l’équipe de conception du site et
l’équipe du Musée de Bretagne pour trouver les meilleurs compromis. Cela est passé par des points hebdomadaires de
revue des maquettes, des échanges de mails et l’utilisation de <a href="https://www.figma.com/">Figma</a>. Cet outil a permis au
Musée de Bretagne et au développeur de commenter au fur et à mesure les interfaces en cours de réalisation pour partager
les textes définitifs, les comportements interactifs ou encore les hyperliens de destination sans se surcharger d’emails.</p>
<p>Les limites de cette démarche d’un point de vue écoconception sont le manque de sobriété de ces outils
(visioconférences, Figma…). Idéalement, l’écoconception consisterait aussi à limiter l’impact environnemental pendant
la co-création. Si <a href="https://penpot.app/">PenPot</a> semble être une alternative Open Source intéressante à Figma, nous n’avons pas trouvé de
façon plus efficiente de collaborer sobrement.</p>
";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:512;s:7:"headers";a:8:{s:6:"server";s:21:"nginx/1.18.0 (Ubuntu)";s:4:"date";s:29:"Mon, 08 Jul 2024 17:57:06 GMT";s:12:"content-type";s:23:"text/xml; charset=utf-8";s:14:"content-length";s:5:"98094";s:13:"last-modified";s:29:"Thu, 23 May 2024 12:49:50 GMT";s:4:"etag";s:16:""664f3b6e-17f2e"";s:35:"content-security-policy-report-only";s:234:"default-src 'self'; img-src 'self' https://cdn.matomo.cloud/itsonus.matomo.cloud/matomo.js; object-src 'none'; script-src 'self' 'unsafe-inline' https://cdn.matomo.cloud/itsonus.matomo.cloud/matomo.js; style-src 'self' 'unsafe-inline'";s:13:"accept-ranges";s:5:"bytes";}s:5:"build";i:1674203855;s:21:"cache_expiration_time";i:1720465025;s:23:"__cache_expiration_time";i:1720465025;}