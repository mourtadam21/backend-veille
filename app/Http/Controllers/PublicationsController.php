<?php

namespace App\Http\Controllers;
use Illuminate\Pagination\LengthAwarePaginator;
use SimplePie\SimplePie;
use GuzzleHttp\Client;

class PublicationsController extends Controller
{
    public function fetchPublications()
    {
        // Liste des flux RSS
        $feeds = [
            'https://ecoinfo.cnrs.fr/category/ecoconception/feed/',
            'http://www.zdnet.fr/blogs/green-si/rss/',
            'https://www.itsonus.fr/blog/feed/feed.xml',
            'http://feeds.feedburner.com/GreenIT'
        ];

    // Création d'un tableau pour stocker les résultats
    $items = [];

    // Parcours des flux
    foreach ($feeds as $feedUrl) {
        // Création d'une nouvelle instance de SimplePie
        $feed = new SimplePie();

        // Définition du répertoire de cache
        $feed->set_cache_location(storage_path('simplepie'));

        // Définition du flux à récupérer
        $feed->set_feed_url($feedUrl);

        // Récupération des éléments du flux
        $feed->init();

        // Parcours des éléments
        foreach ($feed->get_items() as $item) {

            $image = null;
            if ($enclosure = $item->get_enclosure()) {
                $image = $enclosure->get_link();
            }

            // Si aucune image n'est trouvée, on utilise une image par défaut
            if (!$image) {
                $image = 'https://itgreen.eu/wp-content/uploads/2022/01/IT-GREEN.png';
            }

            // Ajout de l'élément au tableau
            $items[] = [
                'title' => $item->get_title(),
                'description' => $item->get_description(),
                'date' => $item->get_date('j F Y | g:i a'),
                'image' => $image,
                'link' => $item->get_link(),
            ];
            
        }

        // Récupération des articles de l'API News
        $client = new Client();
        $response = $client->request('GET', 'https://newsapi.org/v2/everything', [
            'query' => [
                'q' => '"Green IT" OR "technologie verte" OR "informatique durable" OR "numérique responsable"',
                'apiKey' => '4366c5b6d5494dbf831ca3f066878b2b',
                'sortBy' => 'publishedAt',
                'language' => 'fr'
            ]
        ]);

        $data = json_decode($response->getBody(), true);

        foreach ($data['articles'] as $article) {
            $image = $article['urlToImage'];

            if (!$image) {
                $image = 'https://itgreen.eu/wp-content/uploads/2022/01/IT-GREEN.png';
            }

            $date = date('j F Y | g:i a', strtotime($article['publishedAt']));

            $items[] = [
                'title' => $article['title'],
                'description' => $article['description'],
                'date' => $date,
                'image' => $image,
                'link' => $article['url'],
            ];
        }
    }
    // Récupération de la page actuelle
    $page = LengthAwarePaginator::resolveCurrentPage();

    // Création d'une nouvelle instance de LengthAwarePaginator
    $items = new LengthAwarePaginator(
        array_slice($items, ($page - 1) * 9, 9), // Les éléments pour la page actuelle
        count($items), // Le nombre total d'éléments
        9, // Le nombre d'éléments par page
        $page, // La page actuelle
        ['path' => LengthAwarePaginator::resolveCurrentPath()] // Le chemin de la pagination
    );

    // Passage du tableau à la vue
    return view('publications', ['items' => $items]);
        }
    }
?>